/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

 import React from 'react';
 import App from './src';
 import { Provider } from 'react-redux';
 import { PersistGate } from 'redux-persist/integration/react'
 
 import store from './src/services/redux/store';
 import { persistor } from './src/services/redux/store/configureStore';
 
 
 
 
 class DesignTestApp extends React.Component{
 
   render() {
     return (
       <Provider store={store}>
         <PersistGate loading={null} persistor={persistor}>
           <App />
         </PersistGate>
       </Provider>
     )
   }
 
 }
 
 export default DesignTestApp;
 