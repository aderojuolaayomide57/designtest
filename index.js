/**
 * @format
 */

 import {AppRegistry} from 'react-native';
 //import codePush from "react-native-code-push";
 import DesignTestApp from './App';
 import {name as appName} from './app.json';
 
 //let codePushOptions = { checkFrequency: codePush.CheckFrequency.ON_APP_RESUME };
 
 //const DesignTest = codePush(codePushOptions)(DesignTestApp)
 
 
 AppRegistry.registerComponent(appName, () => DesignTestApp);
 
