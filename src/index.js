import React from 'react';
import { NavigationContainer } from "@react-navigation/native";

import { createStackNavigator } from "@react-navigation/stack";
import { navigationRef } from './utils/navigation';
import WeekdayScreen from './screens/Weekdays';
import WeekendScreen from './screens/Weekends';
import Register from './containers/Register';


const Stack = createStackNavigator();


const App = () => {
    return(
        <NavigationContainer ref={navigationRef}>
            <Stack.Navigator initialRouteName="Weekdays">
                <Stack.Screen 
                    name="Weekdays" 
                    component={WeekdayScreen} 
                    options={{
                        headerShown: false
                    }}
                />
                <Stack.Screen 
                    name="Weekends" 
                    component={WeekendScreen} 
                    options={{
                        headerShown: false
                    }}
                />
                <Stack.Screen 
                    name="register" 
                    component={Register} 
                    options={{
                        headerShown: false
                    }}
                />                
            </Stack.Navigator>
        </NavigationContainer>
    )
}

export default App;