//import Config from "react-native-config";
import { initializeApp } from 'firebase/app';
import { getAuth, onAuthStateChanged } from 'firebase/auth';
import { getFirestore, FirestoreSettings, initializeFirestore } from 'firebase/firestore';
import { LogBox } from 'react-native';




const firebaseConfig = {
    /**apiKey: 'AIzaSyB2mT3oMZj8hxA1Cd9c0VTgyIPvyuYkg88',
    authDomain: Config.AUTH_DOMAIN,
    databaseURL: Config.DATABASE_URL,
    projectId: Config.PROJECT_ID,
    storageBucket: Config.STORAGE_BUCKET,
    messagingSenderId: Config.MESSAGING_SENDER_ID,
    appId: Config.ANDROID_APP_ID,**/
    apiKey: 'AIzaSyB2mT3oMZj8hxA1Cd9c0VTgyIPvyuYkg88',
    authDomain: 'eminent-636e4.firebaseapp.com',
    databaseURL: 'https://eminent-636e4-default-rtdb.firebaseio.com/',
    projectId: 'eminent-636e4',
    storageBucket: 'eminent-636e4.appspot.com',
    messagingSenderId: '3588334567',
    //appId: '1:3588334567:ios:ce9943a472eaf446df5342'

};
LogBox.ignoreLogs(['Setting a timer']);

//const theSettings = FirestoreSettings({experimentalForceLongPolling: true});
  

const firebaseApp = initializeApp(firebaseConfig);
const auth = getAuth(firebaseApp);
const db = initializeFirestore(firebaseApp, {experimentalForceLongPolling: true});

export { db, auth }

