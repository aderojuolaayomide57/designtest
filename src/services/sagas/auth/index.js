import { call, put, takeLeading } from 'redux-saga/effects';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { Alert, ToastAndroid, Platform } from 'react-native';

import { Types, Creators } from '../../redux/auth/actions';
import { navigate } from '../../../utils/navigation';
//import firebase from 'firebase';
import {db, auth} from '../../firebase/config';

import { 
    GoogleAuthProvider, 
    getAuth, 
    signInWithPopup, 
    signInWithEmailAndPassword, 
    createUserWithEmailAndPassword, 
    sendPasswordResetEmail, 
    signOut
} from 'firebase/auth';
import { query, getDocs, collection, where, addDoc } from "firebase/firestore";


const usersCollectionRef = collection(db, 'users');


const notifyMessage = (msg) => {
  const message = 'auth/user-not-found' === msg ? 'Wrong email or password' : msg
  if (Platform.OS === 'android') {
    ToastAndroid.show(message, ToastAndroid.CENTER, ToastAndroid.LONG)
  } else {
    Alert.alert(message);
  }
}

const handleAuthentication = () => {
      signOut(auth)
}


  export function* register(actions) {
    try {
      const { data } = actions;
      const {weekdayActivity, email, weekendActivity} = data;
      const password = 'dsfgjjj'
      const register = e => createUserWithEmailAndPassword(auth, email, password)
      const response = yield call (register);
      const newres = addDoc(usersCollectionRef, {
        uid: response.user.uid,
        weekdayActivity: weekdayActivity,
        password: password,
        weekendActivity: weekendActivity,
        email,
        authProvider: "local",
      });
      const user = {uid: response.user.uid,weekdayActivity: weekdayActivity,weekendActivity: weekendActivity, email: email}
      yield put(Creators.registerSuccess({payload: user}));
      //navigate('Home')
      Alert.alert("Profile saved successfully");
    } catch (error) {
      notifyMessage(error.code)
      yield put(Creators.registerFailure(error));
    }
  }
  
export function* watchRegister() {
    yield takeLeading(Types.REGISTER_REQUEST, register);
}


  
