import { all } from 'redux-saga/effects';

import {watchRegister} from './auth';

export default function* rootSaga() {
    yield all([
        watchRegister(),
    ]);
}
