import { createReducer } from 'reduxsauce';
import { persistReducer } from 'redux-persist';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Types from './actionTypes';

export const INITIAL_STATE = {
    data: null,
    error: false,
    isRegistering: false,
    isLogingin: false,
    isLogingOut: false,
}

export const register = (state = INITIAL_STATE, action) => {
    return {...state, isRegistering: true, error: false}
} 

export const registerSuccess = (state = INITIAL_STATE, action) => {
    return {
        ...state,
        user: action.responseData,
        error: false,
        isRegistering: false
    }
}

export const registerFailure = (state = INITIAL_STATE, action) => {
    return {
        ...state,
        error: true,
        error_message: action.error,
        isRegistering: false,
    }
}

export const login = (state = INITIAL_STATE, action) => {
    return {...state, isLogingin: true, error: false}
} 

export const loginSuccess = (state = INITIAL_STATE, action) => {
    return {
        ...state,
        user: action.responseData,
        error: false,
        isLogingin: false
    }
}

export const loginFailure = (state = INITIAL_STATE, action) => {
    return {
        ...state,
        error: true,
        error_message: action.error,
        isLogingin: false,
    }
}

export const logout = (state = INITIAL_STATE, action) => {
    return {...state, isLogingOut: true, error: false}
} 

export const logoutSuccess = (state = INITIAL_STATE, action) => {
    return {
        ...state,
        error: false,
        user: null,
        isLogingOut: false
    }
}

export const logoutFailure = (state = INITIAL_STATE, action) => {
    return {
        ...state,
        error: true,
        error_message: action.error,
        isLogingOut: false,
    }
}



export const HANDLERS = {
    [Types.REGISTER_REQUEST]: register,
    [Types.REGISTER_SUCCESS]: registerSuccess,
    [Types.REGISTER_FAILURE]: registerFailure,

    [Types.LOGIN_REQUEST]: login,
    [Types.LOGIN_SUCCESS]: loginSuccess,
    [Types.LOGIN_FAILURE]: loginFailure,

    [Types.LOGOUT_REQUEST]: logout,
    [Types.LOGOUT_SUCCESS]: logoutSuccess,
    [Types.LOGOUT_FAILURE]: logoutFailure,

}

const persistConfig = {
    key: 'auth',
    storage: AsyncStorage,
    blacklist: ['isRegistering']
  };
  
  const AuthReducer = createReducer(INITIAL_STATE, HANDLERS);
  export default persistReducer(persistConfig, AuthReducer)

