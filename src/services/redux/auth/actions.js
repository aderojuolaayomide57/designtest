import { createActions } from 'reduxsauce';


export const { Types, Creators } = createActions(
    {
        registerRequest: ['data'],
        registerSuccess: ['responseData'],
        registerFailure: ['error'],

        loginRequest: ['data'],
        loginSuccess: ['responseData'],
        loginFailure: ['error'],

        logoutRequest: null,
        logoutSuccess: null,
        logoutFailure: null
    },
    {}
);