import React from 'react';
import { StyleSheet } from 'react-native';
import { BLACK_COLOR, WHITE_COLOR, PRIMARY_COLOR } from '../../styles/colors';



const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: BLACK_COLOR,
        padding: 45,
    },
    HeaderText: {
        color: WHITE_COLOR,
        fontSize: 20,
        fontWeight: "bold",
        marginTop: 40,
        textAlign: 'center'
    },
    logoBox: {
        alignSelf: 'center',
        flexDirection: 'row',
    },
    logoText: {
        fontSize: 38,
        color: WHITE_COLOR,
        marginRight: 10,
        textAlign: 'center'
    },
    logoTextMiddle: {
        color: PRIMARY_COLOR,
        fontSize: 45,
        fontWeight: "bold",
    },
    subheader: {
        color: 'gray',
        fontSize: 15,
        fontWeight: "normal",
        textAlign: 'center',
        marginTop: 20,
    },
    iconSize: {
        padding: 18,
        fontSize: 35,
        marginTop: 10,
        borderRadius: 60,
        borderColor: 'white',
        borderWidth: 2,
        alignSelf: 'center',
        margin: 5.5
    },
    lifestyleText: {
        fontSize: 14,
        color: WHITE_COLOR,
        textAlign: 'center',
        marginTop: 5
    },
    lifestylesbox: {
        flexDirection: 'row',
        alignItems: "flex-start",
        flexWrap: 'wrap',
        justifyContent: 'space-around',
        alignContent: "center",
        paddingBottom: 50,
        marginTop: 5
    },
    btnBox: {
        marginTop: 10,
        marginBottom: 60,
    }
});

export default styles;