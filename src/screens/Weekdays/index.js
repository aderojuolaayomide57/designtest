import React, { Component } from 'react';
import { View, Text, ScrollView } from 'react-native';
import LifeStyle from '../../components/LifeStyleBox';
import CustomButton from '../../components/button';
import ErrorMessage from '../../components/errorMessage';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Foundation from 'react-native-vector-icons/Foundation';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import Feather from 'react-native-vector-icons/Feather';
import Entypo from 'react-native-vector-icons/Entypo';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Octicons from 'react-native-vector-icons/Octicons';




import styles from './styles';


export default class WeekdayScreen extends Component {

    constructor(props){
        super(props);
        this.selectActivity = this.selectActivity.bind(this);
        this.redirect = this.redirect.bind(this);
        this.state = {
            selectedActivity: '',
            error: '',
        }
    }

    selectActivity(selected) {
        this.setState({ selectedActivity: selected});
    }

    redirect(){
        if(this.state.selectedActivity == '') {
            this.setState({error: "Please select a week days activity"})
        }else{
            this.setState({error: ""})
            this.props.navigation.navigate('Weekends', {weekdayActivity: this.state.selectedActivity});
        }
    }



    render() {

        return (
            <ScrollView style={styles.container}>
                <Text style={styles.logoText}>Unexpected things about you can tell us the 
                most about the clothes that you like to wear</Text>
                <Text style={styles.HeaderText}>Monday to Friday</Text>
                <Text style={styles.subheader}>What best describes a typical week for you ?</Text>
                <ErrorMessage errorValue={this.state.error} />
                <View style={styles.lifestylesbox}>
                    <LifeStyle >
                        <FontAwesome5 name="hand-holding-heart" 
                            style={[styles.iconSize, {backgroundColor: this.state.selectedActivity === "Charity" ? 'pink' : 'black'}]} 
                            color= 'white'
                            onPress={() => {
                                this.selectActivity('Charity')
                            }}
                        />
                        <Text style={styles.lifestyleText}>Charity</Text>
                    </LifeStyle>
                    <LifeStyle >
                        <Entypo name='v-card' 
                            style={[styles.iconSize, {backgroundColor: this.state.selectedActivity === "Corporate" ? 'pink' : 'black'}]} 
                            color= 'white'
                            onPress={() => {
                                this.selectActivity('Corporate')
                            }}
                        />
                        <Text style={styles.lifestyleText}>Corporate</Text>
                    </LifeStyle>
                    <LifeStyle >
                        <Entypo name='bowl' 
                            style={[styles.iconSize, {backgroundColor: this.state.selectedActivity === 'Creative' ? 'pink' : 'black'}]} 
                            color= 'white'
                            onPress={() => {
                                this.selectActivity('Creative')
                            }}
                        />
                        <Text style={styles.lifestyleText}>Creative</Text>
                    </LifeStyle>
                    <LifeStyle >
                        <Ionicons name='md-musical-notes-outline' 
                            style={[styles.iconSize, {backgroundColor: this.state.selectedActivity === 'Media' ? 'pink' : 'black'}]} 
                            color= 'white'
                            onPress={() => {
                                this.selectActivity('Media')
                            }}
                        />
                        <Text style={styles.lifestyleText}>Media</Text>
                    </LifeStyle>
                    <LifeStyle >

                        <Octicons name='plus-circle' 
                            style={[styles.iconSize, {backgroundColor: this.state.selectedActivity === 'HealthCare' ? 'pink' : 'black'}]} 
                            color= 'white'
                            onPress={() => {
                                this.selectActivity('HealthCare')
                            }}
                        />
                        <Text style={styles.lifestyleText}>HealthCare</Text>
                    </LifeStyle>
                    <LifeStyle >
                        <MaterialCommunityIcons name='teddy-bear' 
                            style={[styles.iconSize, {backgroundColor: this.state.selectedActivity === 'Kids' ? 'pink' : 'black'}]} 
                            color= 'white'
                            onPress={() => {
                                this.selectActivity('Kids')
                            }}
                        />
                        <Text style={styles.lifestyleText}>Kids</Text>
                    </LifeStyle>
                    <LifeStyle >
                        <FontAwesome5 name='handshake' 
                            style={[styles.iconSize, {backgroundColor: this.state.selectedActivity === 'Meetings' ? 'pink' : 'black'}]} 
                            color= 'white'
                            onPress={() => {
                                this.selectActivity('Meetings')
                            }}
                        />
                       <Text style={styles.lifestyleText}>Meetings</Text>
                    </LifeStyle>
                    <LifeStyle >
                        <MaterialCommunityIcons name='palm-tree'
                            style={[styles.iconSize, {backgroundColor: this.state.selectedActivity === 'Outdoor' ? 'pink' : 'black'}]} 
                            color= 'white'
                            onPress={() => {
                                this.selectActivity('Outdoor')
                            }}
                        />
                        <Text style={styles.lifestyleText}>Outdoor</Text>
                    </LifeStyle>
                    <LifeStyle >
                        <Entypo name='lab-flask'
                            style={[styles.iconSize, {backgroundColor: this.state.selectedActivity === 'Research' ? 'pink' : 'black'}]} 
                            color= 'white'
                            onPress={() => {
                                this.selectActivity('Research')
                            }}
                        />
                        <Text style={styles.lifestyleText}>Research</Text>
                    </LifeStyle>
                    <LifeStyle >
                        <Ionicons name='print-outline'
                            style={[styles.iconSize, {backgroundColor: this.state.selectedActivity === 'Retail' ? 'pink' : 'black'}]} 
                            color= 'white'
                            onPress={() => {
                                this.selectActivity('Retail')
                            }}
                        />
                        <Text style={styles.lifestyleText}>Retail</Text>
                    </LifeStyle>
                    <LifeStyle >
                        <SimpleLineIcons name='graduation'
                            style={[styles.iconSize, {backgroundColor: this.state.selectedActivity === 'Studying' ? 'pink' : 'black'}]} 
                            color= 'white'
                            onPress={() => {
                                this.selectActivity('Studying')
                            }}
                        />
                        <Text style={styles.lifestyleText}>Studying</Text>
                    </LifeStyle>
                    <LifeStyle >
                        <Entypo name='blackboard'
                            style={[styles.iconSize, {backgroundColor: this.state.selectedActivity === 'Teaching' ? 'pink' : 'black'}]} 
                            color= 'white'
                            onPress={() => {
                                this.selectActivity('Teaching')
                            }}
                        />
                        <Text style={styles.lifestyleText}>Teaching</Text>
                    </LifeStyle>
                    <LifeStyle >
                        <MaterialCommunityIcons name='rocket-outline'
                            style={[styles.iconSize, {backgroundColor: this.state.selectedActivity === 'Technology' ? 'pink' : 'black'}]} 
                            color= 'white'
                            onPress={() => {
                                this.selectActivity('Technology')
                            }}
                        />
                        <Text style={styles.lifestyleText}>Technology</Text>
                    </LifeStyle>
                    <LifeStyle >
                        <MaterialCommunityIcons name='weight-lifter'
                            style={[styles.iconSize, {backgroundColor: this.state.selectedActivity === 'Training' ? 'pink' : 'black'}]} 
                            color= 'white'
                            onPress={() => {
                                this.selectActivity('Training')
                            }}
                        />
                        <Text style={styles.lifestyleText}>Training</Text>
                    </LifeStyle>
                    <LifeStyle >
                        <MaterialCommunityIcons name='human-capacity-increase'
                            style={[styles.iconSize, {backgroundColor: this.state.selectedActivity === 'Home Office' ? 'pink' : 'black'}]} 
                            color= 'white'
                            onPress={() => {
                                this.selectActivity('Home Office')
                            }}
                        />
                        <Text style={styles.lifestyleText}>Home Office</Text>
                    </LifeStyle>


                </View>
                <View style={styles.btnBox}>
                    <CustomButton
                        type="secondary"
                        icon={true}
                        onPress={() => this.redirect()}
                    />
                </View>
            </ScrollView>
        )
    }
}
