import React, { Component } from 'react';
import { View, Image, Text, ScrollView } from 'react-native';
import LifeStyle from '../../components/LifeStyleBox';
import CustomButton from '../../components/button';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import ErrorMessage from '../../components/errorMessage';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Foundation from 'react-native-vector-icons/Foundation';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import Feather from 'react-native-vector-icons/Feather';
import Entypo from 'react-native-vector-icons/Entypo';
import FontAwesome from 'react-native-vector-icons/FontAwesome';




import styles from './styles';


export default class WeekendScreen extends Component {

    constructor(props){
        super(props);
        this.selectActivity = this.selectActivity.bind(this);
        this.redirect = this.redirect.bind(this);
        this.state = {
            selectedActivity: '',
            error: '',
        }
    }

    selectActivity(selected) {
        this.setState({ selectedActivity: selected});
    }

    redirect(){
        if(this.state.selectedActivity == '') {
            this.setState({error: "Please select a weekend activity"})
        }else{
            this.setState({error: ""})
            this.props.navigation.navigate('register', {
                weekendActivity: this.state.selectedActivity,
                weekdayActivity: this.props.route.params.weekdayActivity
            });
        }
    }



    render() {
          return (
            <ScrollView style={styles.container}>
                <Text style={styles.HeaderText}>Saturday to Sunday</Text>
                <Text style={styles.subheader}>What best describes a typical weekend ?</Text>
                <ErrorMessage errorValue={this.state.error} />
                <View style={styles.lifestylesbox}>
                    <LifeStyle >
                        <MaterialIcons name='fastfood' 
                            style={[styles.iconSize, {backgroundColor: this.state.selectedActivity === 'Brunch' ? 'pink' : 'black'}]} 
                            color= 'white'
                            onPress={() => {
                                this.selectActivity('Brunch')
                            }}
                        />
                            <Text style={styles.lifestyleText}>Brunch</Text>
                    </LifeStyle>

                    <LifeStyle >
                            <MaterialCommunityIcons name='city-variant-outline'
                                style={[styles.iconSize, {backgroundColor: this.state.selectedActivity === 'City Walks' ? 'pink' : 'black'}]} 
                                color= 'white'
                                onPress={() => {
                                    this.selectActivity('City Walks')
                                }}
                            />
                                <Text style={styles.lifestyleText}>City Walks</Text>
                    </LifeStyle>

                    <LifeStyle >                 
                        <FontAwesome name='coffee'
                            style={[styles.iconSize, {backgroundColor: this.state.selectedActivity === 'Coffees' ? 'pink' : 'black'}]} 
                            color= 'white'
                            onPress={() => {
                                this.selectActivity('Coffees')
                            }}
                        />
                        <Text style={styles.lifestyleText}>Coffees</Text>
                    </LifeStyle>

                    <LifeStyle >
                        <MaterialCommunityIcons name='pot-steam'
                            style={[styles.iconSize, {backgroundColor: this.state.selectedActivity === 'Cooking' ? 'pink' : 'black'}]} 
                            color= 'white'
                            onPress={() => {
                                this.selectActivity('Cooking')
                            }}
                        />
                        <Text style={styles.lifestyleText}>Cooking</Text>
                    </LifeStyle>

                    <LifeStyle >
                        <FontAwesome5 name='glass-cheers'
                            style={[styles.iconSize, {backgroundColor: this.state.selectedActivity === 'Date Nights' ? 'pink' : 'black'}]} 
                            color= 'white'
                            onPress={() => {
                                this.selectActivity('Date Nights')
                            }}
                        />
                        <Text style={styles.lifestyleText}>Date Nights</Text>
                    </LifeStyle>

                    <LifeStyle >
                        <MaterialCommunityIcons name='brush-variant'
                            style={[styles.iconSize, {backgroundColor: this.state.selectedActivity === 'DIY' ? 'pink' : 'black'}]} 
                            color= 'white'
                            onPress={() => {
                                this.selectActivity('DIY')
                            }}
                        />
                        <Text style={styles.lifestyleText}>DIY</Text>
                    </LifeStyle>

                    <LifeStyle >
                        <MaterialIcons name='family-restroom'
                            style={[styles.iconSize, {backgroundColor: this.state.selectedActivity === 'Family Time' ? 'pink' : 'black'}]} 
                            color= 'white'
                            onPress={() => {
                                this.selectActivity('Family Time')
                            }}
                        />
                        <Text style={styles.lifestyleText}>Family Time</Text>
                    </LifeStyle>

                    <LifeStyle >
                        <Entypo name='flower'
                            style={[styles.iconSize, {backgroundColor: this.state.selectedActivity === 'Gardening' ? 'pink' : 'black'}]} 
                            color= 'white'
                            onPress={() => {
                                this.selectActivity('Gardening')
                            }}
                        />
                        <Text style={styles.lifestyleText}>Gardening</Text>
                    </LifeStyle>
                    <LifeStyle >
                            <FontAwesome name='cutlery'
                                style={[styles.iconSize, {backgroundColor: this.state.selectedActivity === 'Lunches' ? 'pink' : 'black'}]} 
                                color= 'white'
                                onPress={() => {
                                    this.selectActivity('Lunches')
                                }}
                            />
                        <Text style={styles.lifestyleText}>Lunches</Text>
                    </LifeStyle>
                    <LifeStyle >
                            <MaterialCommunityIcons name='sofa-outline'
                                style={[styles.iconSize, {backgroundColor: this.state.selectedActivity === 'Netflix' ? 'pink' : 'black'}]} 
                                color= 'white'
                                onPress={() => {
                                    this.selectActivity('Netflix')
                                }}
                            />
                        <Text style={styles.lifestyleText}>Netflix</Text>
                    </LifeStyle>
                    <LifeStyle >
                            <Foundation name='mountains'
                                style={[styles.iconSize, {backgroundColor: this.state.selectedActivity === 'Outdoor' ? 'pink' : 'black'}]} 
                                color= 'white'
                                onPress={() => {
                                    this.selectActivity('Outdoor')
                                }}
                            />
                        <Text style={styles.lifestyleText}>Outdoor</Text>
                    </LifeStyle>
                    <LifeStyle >
                        <SimpleLineIcons name='globe'
                            style={[styles.iconSize, {backgroundColor: this.state.selectedActivity === 'Party' ? 'pink' : 'black'}]} 
                            color= 'white'
                            onPress={() => {
                                this.selectActivity('Party')
                            }}
                        />
                        <Text style={styles.lifestyleText}>Party</Text>
                    </LifeStyle>
                    <LifeStyle >
                        <Feather name='book-open'
                            style={[styles.iconSize, {backgroundColor: this.state.selectedActivity === 'Reading' ? 'pink' : 'black'}]} 
                            color= 'white'
                            onPress={() => {
                                this.selectActivity('Reading')
                            }}
                        />
                        <Text style={styles.lifestyleText}>Reading</Text>
                    </LifeStyle>
                    <LifeStyle >
                        <MaterialCommunityIcons name='mother-heart'
                            style={[styles.iconSize, {backgroundColor: this.state.selectedActivity === 'Selfcare' ? 'pink' : 'black'}]} 
                            color= 'white'
                            onPress={() => {
                                this.selectActivity('Selfcare')
                            }}
                        />
                        <Text style={styles.lifestyleText}>Selfcare</Text>
                    </LifeStyle>
                    <LifeStyle >
                        <MaterialIcons name='self-improvement'
                            style={[styles.iconSize, {backgroundColor: this.state.selectedActivity === 'Social Detox' ? 'pink' : 'black'}]} 
                            color= 'white'
                            onPress={() => {
                                this.selectActivity('Social Detox')
                            }}
                        />
                        <Text style={styles.lifestyleText}>Social Detox</Text>
                    </LifeStyle>
                    <LifeStyle >
                        <MaterialIcons name='luggage'
                            style={[styles.iconSize, {backgroundColor: this.state.selectedActivity === 'Traveling' ? 'pink' : 'black'}]} 
                            color= 'white'
                            onPress={() => {
                                this.selectActivity('Traveling')
                            }}
                        />
                        <Text style={styles.lifestyleText}>Traveling</Text>
                    </LifeStyle>
                    <LifeStyle >
                        <MaterialCommunityIcons name='weight-lifter'
                            style={[styles.iconSize, {backgroundColor: this.state.selectedActivity === 'Working Out' ? 'pink' : 'black'}]} 
                            color= 'white'
                            onPress={() => {
                                this.selectActivity('Working Out')
                            }}
                        />
                        <Text style={styles.lifestyleText}>Working Out</Text>
                    </LifeStyle>
                    <LifeStyle >
                        <MaterialIcons name='work'
                            style={[styles.iconSize, {backgroundColor: this.state.selectedActivity === 'Work' ? 'pink' : 'black'}]} 
                            color= 'white'
                            onPress={() => {
                                this.selectActivity('Work')
                            }}
                        />
                        <Text style={styles.lifestyleText}>Work</Text>
                    </LifeStyle>



                </View>
                <View style={styles.btnBox}>
                    <CustomButton
                        type="secondary"
                        icon={true}
                        onPress={() => this.redirect()}
                    />
                </View>
            </ScrollView>
        )
    }
}
