import React from 'react';
import { View, Text, ScrollView } from 'react-native';
import CustomButton from '../../components/button';
import CustomTextInput from '../../components/textInput';
import ErrorMessage from '../../components/errorMessage';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { Formik } from 'formik'
import * as Yup from 'yup';

const validationSchema = Yup.object().shape({
    email: Yup.string()
      .label('Email')
      .email('Enter a valid email')
      .required('Email is required'),
})



import styles from './styles';


const RegisterScreen = (props) => {
        const initialValues = {
            email: '',
            weekendActivity: props.route.params.weekendActivity,
            weekdayActivity: props.route.params.weekdayActivity
        }
        //console.log(initialValues);
        return (
            <ScrollView style={styles.container}>
                <Text style={styles.subheader}>How does it feels? Exciting</Text>
                <Text style={styles.logoText}>We're almost done! 
                Let's not loose our progress and save your information right here.
                Leave your email and save your profile</Text>
                <Text style={styles.subheader}>To recieve your first selection, subscribe at the next steps
                and share your delivery informations.
                Once your profile is saved you can always decide to subscribe later by conforming your email address at the cheeckbox</Text>
                <Text style={styles.HeaderText}>Type your email address to save your profile</Text>

                <Formik
                    initialValues={initialValues}
                    onSubmit={values => props.registerUser(values)}
                    validationSchema={validationSchema}
                >
                    {formikProps => (
                        <React.Fragment>
                            <View style={styles.btnBox}>
                                <CustomTextInput
                                    name="email"
                                    label="Email"
                                    placeholder="Type your email"
                                    onChangeText={formikProps.handleChange('email')}
                                    value={formikProps.values.email}
                                    noTopBorder={true}
                                    keyboardType="email-address"
                                />
                                <ErrorMessage errorValue={formikProps.errors.email} />
                                <CustomButton
                                    type="primary"
                                    icon={false}
                                    text='SAVE'
                                    onPress={formikProps.handleSubmit}
                                />
                            </View>
                        </React.Fragment>
                    )}
                </Formik>


            </ScrollView>
        )
}

export default RegisterScreen;
