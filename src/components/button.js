import React from 'react';
import { TouchableOpacity, StyleSheet, Text } from 'react-native'
import PropTypes from 'prop-types';
import AntDesign from 'react-native-vector-icons/AntDesign';

import { 
    PRIMARY_COLOR, 
} from '../styles/colors';

const styles = StyleSheet.create({
    baseBtn: {
        borderRadius: 10,
        padding: 15,
    },
    addIcon: {
        
    },
    baseTxt: {
        fontFamily: 'Avenir',
        fontStyle: 'normal',
        fontWeight: 'bold',
        fontSize: 16,
        lineHeight: 22,
        textAlign: 'center',
        letterSpacing: 0.33913
    },

    primaryBtn: {
        borderWidth: 2,
        borderColor: PRIMARY_COLOR,
        borderRadius: 45,
    },
    primaryText: {
        color: PRIMARY_COLOR
    },

    secondaryBtn: {
        backgroundColor: PRIMARY_COLOR,
        borderRadius: 60,
        borderWidth: 2,
        alignSelf: 'center',
        margin: 3,
        padding: 15,
    },
    secondaryText: {
        color: 'black'
    },  
    iconSize: {
        fontSize: 30,
        marginTop: 1,
    },
  
})

class PrimaryButton {
    constructor() {
        this.buttonType = 'primary'
    }

    isMatch(buttonType) {
        return buttonType === this.buttonType
    }

    buttonStyle() {
        return {
            btn: styles.primaryBtn,
            text: styles.primaryText
        }
    }
}

class SecondaryButton {
    constructor() {
        this.buttonType = 'secondary'
    }

    isMatch(buttonType) {
        return buttonType === this.buttonType
    }

    buttonStyle() {
        return {
            btn: styles.secondaryBtn,
            text: styles.secondaryText
        }
    }
}



const CustomButton = (props) => {
    const buttonVarieties = [
        new PrimaryButton(),
        new SecondaryButton(),
    ]
    let selectedButton;
    for (const button of buttonVarieties) {
        if (button.isMatch(props.type)) {
            selectedButton = button.buttonStyle();
            break;
        }
    }
    return (
        <TouchableOpacity
            onPress={props.onPress}
            style={[styles.baseBtn, selectedButton.btn, props.style.btn, props.disabled && styles.disabledStyle]}
            disabled={props.disabled}
        >
            {!props.icon && <Text style={[styles.baseTxt, selectedButton.text, props.style.text]}>{props.text}</Text> }
            {props.icon && <AntDesign name="arrowdown" style={styles.iconSize} />}
        </TouchableOpacity>
    )
}

CustomButton.propTypes = {
    type: PropTypes.oneOf(['primary', 'secondary']).isRequired,
    text: PropTypes.string,
    onPress: PropTypes.func.isRequired,
    style: PropTypes.shape({}),
    disabled: PropTypes.bool,
    icon: PropTypes.bool,
}


CustomButton.defaultProps = {
    style: {
        btn: null,
        text: null
    },
    disabled: false,
}

export default CustomButton;
