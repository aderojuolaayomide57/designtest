import React from 'react';
import { TextInput, View, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';

import { BORDER_COLOR, LIGHT_BORDER_COLOR } from '../styles/colors';


const styles = StyleSheet.create({
    inputWrapper: {
        borderRadius: 8,
        paddingTop: 20,
        paddingHorizontal: 5,
        fontFamily: 'Avenir',
        fontStyle: 'normal',
        fontWeight: '500',
        fontSize: 13,
        lineHeight: 16,
        letterSpacing: 0.74,
        textAlignVertical: 'top',
        paddingBottom: 5,
        backgroundColor: 'white',
        paddingLeft: 15
    },
    container: {
        marginBottom: 20
    }
})

const CustomTextInput = (props) => {
    return (
        <View style={styles.container}>
            <TextInput
                style={[styles.inputWrapper]}
                placeholder={props.placeholder}
                placeholderTextColor={LIGHT_BORDER_COLOR}
                onChangeText={props.onChangeText}
                value={props.value}
                keyboardType={props.keyboardType}
                secureTextEntry={props.secureTextEntry}
                editable={!props.disabled}
                multiline={props.multiline}
                numberOfLines={props.numberOfLines}
            />
        </View>
    )
}

CustomTextInput.propTypes = {
    label: PropTypes.string.isRequired,
    onChangeText: PropTypes.func.isRequired,
    placeholder: PropTypes.string.isRequired,
    value: PropTypes.string,
    keyboardType: PropTypes.oneOf(['default',
                                  'number-pad',
                                  'decimal-pad',
                                  'numeric',
                                  'email-address',
                                  'phone-pad']),
    secureTextEntry: PropTypes.bool,
    multiline: PropTypes.bool,
    numberOfLines: PropTypes.number,

}

CustomTextInput.defaultProps = {
    value: null,
    keyboardType: 'default',
    secureTextEntry: false,
    multiline: false,
}

export default CustomTextInput;