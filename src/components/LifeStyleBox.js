import React, {} from 'react';
import {View, StyleSheet} from 'react-native';



const LifeStyle = ({children}) => {
    return (
        <View style={styles.lifestyle}>
            {children}
        </View>
    )
}


const styles = StyleSheet.create({
    lifestyle: {
        color: 'white',
        fontWeight: "bold",
    },
})

export default LifeStyle;

